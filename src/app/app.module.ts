import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './shared/footer/footer.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';


const config = {
    apiKey: "AIzaSyD0UQ7C0VjwkBxhznHOTqJmbLx2bmzSCTs",
    authDomain: "job-test-d03cb.firebaseapp.com",
    projectId: "job-test-d03cb",
    storageBucket: "job-test-d03cb.appspot.com",
    messagingSenderId: "614560345853",
    appId: "1:614560345853:web:6bb9920427896660cad894"
  };

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
