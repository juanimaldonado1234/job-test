import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public form: FormGroup;
  numberPattern = /^[\s()+-]([0-9][\s()+-]){6,20}$/

  constructor(private _db: AngularFirestore) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required,Validators.email]),
      country: new FormControl('Argentina', Validators.required),
      phone: new FormControl('', [Validators.required, Validators.min(0)]),
      job: new FormControl('', Validators.required)
    })
  }

  submit(){
    if(this.form.valid){
      const data = {
        name: this.form.controls['name'].value,
        lastname: this.form.controls['lastname'].value,
        email: this.form.controls['email'].value,
        country: this.form.controls['country'].value,
        phone: this.form.controls['phone'].value,
        job: this.form.controls['job'].value,
      }
      this._db.collection('inscriptions').add(data).then(() => {
        this.form.reset();
        alert('Formulario Enviado');
      })
    }else{
      this.form.markAllAsTouched();
    }
  }

}
